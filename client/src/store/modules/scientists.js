import Vue from 'vue'

export default {
    namespaced: true,

    state: {
        scientists: []
    },

    mutations: {
        setScientists(state, data) {
            state.scientists = data;
        },

        addScientist(state, newScientist) {
            state.scientists.splice(0, 0, newScientist);
        },

        deleteScientist(state, id) {
            const i = state.scientists.findIndex(element => element.id === id);
            state.scientists.splice(i, 1);
        },

        updateScientist(state, obj) {
            const i = state.scientists.findIndex(element => element.id === obj.id);
            state.scientists.splice(i, 1, obj);
        },

        upsertScientist(state, obj) {
            const i = state.scientists.findIndex(element => element.id === obj.id);
            if (i == -1) {
                // Not found, insert
                this.addScientist(obj);
            } else {
                // Found, update
                state.scientists.splice(i, 1, obj);
            }
        },
    },

    actions: {
        async pullScientists({ commit }) {
            try {
                const { data } = await Vue.$axios.get('/scientists');
                commit("setScientists", data);
            } catch(error) {
                alert(error);
            }
        },

        async pullOne({ commit }, id) {
            try {
                const { data } = await Vue.$axios.get(`/scientists/${id}`);
                commit("upsertScientist", data);
            } catch(error) {
                alert(error);
            }
        },

        async addScientist({ commit }, payload) {
            try {
                const { data } = await Vue.$axios.post('/scientists', payload);
                // Upload image if requested
                if (payload.imgToUpload) {
                    let formData = new FormData();
                    formData.append("image", payload.imgToUpload);
                    
                    const { data: path } = await Vue.$axios.post(`/scientists/uploadImg/${data.id}`, formData);
                    data.image = path;
                }
                commit("addScientist", data);
            } catch(error) {
                alert(error);
            }
        },

        async deleteScientist({ commit }, id) {
            try {
                await Vue.$axios.delete(`/scientists/${id}`);
                commit("deleteScientist", id);
            } catch(error) {
                alert(error);
            }
        },

        async updateScientist({ commit }, payload) {
            try {
                // Upload image if requested
                if (payload.imgToUpload) {
                    let formData = new FormData();
                    formData.append("image", payload.imgToUpload);
                    
                    await Vue.$axios.post(`/scientists/uploadImg/${payload.id}`, formData);
                    delete payload.image;
                }
                const { data } = await Vue.$axios.patch(`/scientists`, payload);
                commit("updateScientist", data);
            } catch(error) {
                alert(error);
            }
        },
    },

    getters: {
        getScientists: (state) => state.scientists
    }
}
