import Vue from 'vue'

export default {
    namespaced: true,

    state: {
        museums: []
    },

    mutations: {
        setMuseums(state, data) {
            state.museums = data;
        },

        addMuseum(state, newMuseum) {
            state.museums.splice(0, 0, newMuseum);
        },

        deleteMuseum(state, id) {
            const i = state.museums.findIndex(element => element.id === id);
            state.museums.splice(i, 1);
        },

        updateMuseum(state, obj) {
            const i = state.museums.findIndex(element => element.id === obj.id);
            state.museums.splice(i, 1, obj);
        },

        upsertMuseum(state, obj) {
            const i = state.museums.findIndex(element => element.id === obj.id);
            if (i == -1) {
                // Not found, insert
                this.addMuseum(obj);
            } else {
                // Found, update
                state.museums.splice(i, 1, obj);
            }
        }
    },

    actions: {
        async pullMuseums({ commit }) {
            try {
                const { data } = await Vue.$axios.get('/museums');
                commit("setMuseums", data);
            } catch(error) {
                alert(error);
            }
        },

        async pullOne({ commit }, id) {
            try {
                const { data } = await Vue.$axios.get(`/museums/${id}`);
                commit("upsertMuseum", data);
            } catch(error) {
                alert(error);
            }
        },

        async addMuseum({ commit }, payload) {
            try {
                const { data } = await Vue.$axios.post('/museums', payload);
                // Upload image if requested
                if (payload.imgToUpload) {
                    let formData = new FormData();
                    formData.append("image", payload.imgToUpload);
                    
                    const { data: path } = await Vue.$axios.post(`/museums/uploadImg/${data.id}`, formData);
                    data.image = path;
                }
                commit("addMuseum", data);
            } catch(error) {
                alert(error);
            }
        },

        async deleteMuseum({ commit }, id) {
            try {
                await Vue.$axios.delete(`/museums/${id}`);
                commit("deleteMuseum", id);
            } catch(error) {
                alert(error);
            }
        },

        async updateMuseum({ commit }, payload) {
            try {
                // Upload image if requested
                if (payload.imgToUpload) {
                    let formData = new FormData();
                    formData.append("image", payload.imgToUpload);
                    
                    await Vue.$axios.post(`/museums/uploadImg/${payload.id}`, formData);
                    delete payload.image;
                }
                const { data } = await Vue.$axios.patch(`/museums`, payload);
                commit("updateMuseum", data);
            } catch(error) {
                alert(error);
            }
        },

        async updateLink({ commit }, { museumId, scientistId }) {
            try {
                await Vue.$axios.patch(`/museums/link`, { museumId, scientistId });
                commit;
            } catch(error) {
                alert(error);
            }
        },
    },

    getters: {
        getMuseums: (state) => state.museums
    }
}
