export default {
    namespaced: true,

    state: {
        buttons: []
    },

    mutations: {
        setButtons(state, newBtns) {
            state.buttons = newBtns;
        }
    },

    getters: {
        getButtons: (state) => state.buttons
    }
}
