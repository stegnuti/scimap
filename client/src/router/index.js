import Vue from 'vue'
import VueRouter from 'vue-router'
import MapView from '../views/MapView.vue'
import TableView from '../views/TableView.vue'
import TestView from "../views/TestView.vue"
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'MapView',
    component: MapView
  },
  {
    path: '/table',
    name: 'TableView',
    component: TableView
  },
  {
    path: '/test',
    name: 'TestView',
    component: TestView
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
