import Scientist from "../models/Scientist";
import Invention from "../models/Invention";
import Museum from "../models/Museum";

class ScientistService {
    public async getAll(): Promise<any> {
        return await Scientist.findAll({
            include: [
                { model: Invention, as: "inventions" },
                { model: Museum, as: "museum" }
            ]
        });
    }

    public async getOne(id: number): Promise<any> {
        const sci: any = await Scientist.findOne({ 
            where: { id },
            include: [
                { model: Invention, as: "inventions" },
                { model: Museum, as: "museum" }
            ]
        });
        if (sci == null) throw "Scientist with given ID doesn't exist.";
        return sci;
    }

    public async create(sciPayload: any): Promise<any> {
        const sci = await Scientist.create(sciPayload);
        return sci;
    }

    public async delete(id: number): Promise<any> {
        await Invention.destroy({ where: { scientistId: id } });
        await Scientist.destroy({ where: { id } });
    }

    public async setImage(scientistId: any, image: any): Promise<any> {
        let path = `./images/scientists/${scientistId}.jpg`;
        await image.mv(path);
        path = path.slice(1);
        let sci: any = await Scientist.findByPk(scientistId);
        sci.image = path;
        await sci.save();
        return path;
    }

    public async update(sciPayload: any): Promise<any> {
        // Delete inventions that are not in sciPayload
        let all: any = await Invention.findAll({ where: { scientistId: sciPayload.id } });
        for (let inv of all) {
            if (!sciPayload.inventions.find((el: any) => el.id === inv.id)) {
                await Invention.destroy({ where: { id: inv.id }});
            }
        }
        
        // Upsert the rest
        for (let inv of sciPayload.inventions) {
            if (inv.id) {
                await Invention.upsert(inv);
            } else {
                await Invention.upsert({
                    name: inv.name,
                    scientistId: sciPayload.id
                });
            }
        }

        await Scientist.update(sciPayload, { where: { id: sciPayload.id } });
        return await this.getOne(sciPayload.id);
    }
}

export default new ScientistService;