import Museum from "../models/Museum";
import Scientist from "../models/Scientist";

class MuseumService {
    public async getAll(): Promise<any> {
        return await Museum.findAll({
            include: [
                { model: Scientist, as: "showcasedScientist" }
            ]
        });
    }

    public async getOne(id: number): Promise<any> {
        const museum = await Museum.findOne({ 
            where: { id },
            include: [
                { model: Scientist, as: "showcasedScientist" }
            ]
        });
        if (museum == null) throw "Museum with given ID doesn't exist";
        return museum;
    }

    public async create(musPayload: any): Promise<any> {
        const museum = await Museum.create(musPayload);
        return museum;
    }

    public async delete(id: number): Promise<any> {
        await Museum.destroy({ where: { id } });
    }

    public async update(musPayload: any): Promise<any> {
        await Museum.update(musPayload, { where: { id: musPayload.id } });
        return await this.getOne(musPayload.id);
    }

    public async setImage(museumId: any, image: any): Promise<any> {
        let path = `./images/museums/${museumId}.jpg`;
        await image.mv(path);
        path = path.slice(1);
        let mus: any = await Museum.findByPk(museumId);
        mus.image = path;
        await mus.save();
        return path;
    }

    public async updateLink(museumId: any, scientistId: any) {
        // Delete old link
        let mus: any = await Museum.findOne({ where: { showcasedScientistId: scientistId } });
        if (mus != null) {
            mus.showcasedScientistId = null;
            await mus.save();
        }

        // Make new link
        mus = await Museum.findByPk(museumId);
        mus.showcasedScientistId = scientistId;
        await mus.save();
    }
}

export default new MuseumService;