import express from "express";
import MuseumController from "../controllers/MuseumController";
const fileUpload = require("express-fileupload");

const router = express.Router();

router.get("/", MuseumController.getAll);
router.post("/", MuseumController.create);
router.post("/uploadImg/:id", fileUpload(), MuseumController.uploadImg);
router.get("/:id", MuseumController.getOne);
router.patch("/", MuseumController.update);
router.patch("/link", MuseumController.updateLink);
router.delete("/:id", MuseumController.delete);

export default router;