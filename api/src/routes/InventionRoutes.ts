import express from "express";
import InventionController from "../controllers/InventionController";

const router = express.Router();

router.get("/", InventionController.getAll);
router.post("/", InventionController.create);
router.get("/:id", InventionController.getOne);
router.patch("/", InventionController.update);
router.delete("/:id", InventionController.delete);

export default router;