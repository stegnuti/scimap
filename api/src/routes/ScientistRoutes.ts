import express from "express";
import ScientistController from "../controllers/ScientistController";
const fileUpload = require("express-fileupload");

const router = express.Router();

router.get("/", ScientistController.getAll);
router.post("/", ScientistController.create);
router.post("/uploadImg/:id", fileUpload(), ScientistController.uploadImg);
router.get("/:id", ScientistController.getOne);
router.patch("/", ScientistController.update);
router.delete("/:id", ScientistController.delete);

export default router;