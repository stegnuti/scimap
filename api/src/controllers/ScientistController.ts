import ScientistService from "../services/ScientistService";

class ScientistController {
    public async getAll(req: any, res: any) {
        try {
            const scientists = await ScientistService.getAll();
            return res.send(scientists);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async getOne(req: any, res: any) {
        try {
            const scientist = await ScientistService.getOne(req.params["id"]);
            return res.send(scientist);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async create(req: any, res: any) {
        try {
            const scientist = await ScientistService.create(req.body);
            return res.send(scientist);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async delete(req: any, res: any) {
        try {
            await ScientistService.delete(req.params["id"]);
            return res.send();
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async update(req: any, res: any) {
        try {
            const scientist = await ScientistService.update(req.body);
            return res.send(scientist);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }

    public async uploadImg(req: any, res: any) {
        try {
            if (!req.files || Object.keys(req.files).length === 0) {
                return res.status(400).send('No files were uploaded.');
            }

            const path = await ScientistService.setImage(req.params["id"], req.files.image);
            return res.send(path);
        } catch (error) {
            return res.status(500).send(error.message);
        }
    }
}

export default new ScientistController;