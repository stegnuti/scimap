import db from "./database";
import { Model, STRING, INTEGER, DOUBLE, BOOLEAN } from "sequelize";
import Museum from "./Museum";

class Scientist extends Model {
  public id!: number;
  public label!: string;
  public firstName!: string;
  public lastName!: string;
  public yearOfBirth!: number;
  public yearOfDeath!: number;
  public placeOfBirth!: string;
  public POBLat!: number; // Latitude on map
  public POBLng!: number; // Longitude on map
  public nationality!: string;
  public science!: string;
  public nobelPrize!: boolean;
  public image!: string;
}

Scientist.init(
  {
    id: {
      type: INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    label: {
      type: STRING,
      allowNull: false,
      unique: true,
    },
    firstName: {
        type: STRING,
        allowNull: false
    },
    lastName: {
        type: STRING,
        allowNull: false
    },
    yearOfBirth: {
        type: INTEGER,
        allowNull: false
    },
    yearOfDeath: {
        type: INTEGER,
        allowNull: true
    },
    placeOfBirth: {
        type: STRING,
        allowNull: false
    },
    POBLat: {
      type: DOUBLE,
      allowNull: false
    },
    POBLng: {
      type: DOUBLE,
      allowNull: false
    },
    nationality: {
        type: STRING,
        allowNull: false
    },
    science: {
        type: STRING,
        allowNull: false
    },
    nobelPrize: {
        type: BOOLEAN,
        allowNull: false
    },
    image: {
      type: STRING,
      allowNull: true
    }
  },
  {
    timestamps: false,
    sequelize: db,
    tableName: "scientists",
  }
);

export default Scientist;