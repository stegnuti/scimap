import db from "./database";
import Scientist from "./Scientist";
import { Model, STRING, INTEGER, DATE, BOOLEAN } from "sequelize";

class Invention extends Model {
  public id!: number;
  public scientistId!: number;
  public name!: string;
}

Invention.init(
  {
    id: {
      type: INTEGER.UNSIGNED,
      primaryKey: true,
      autoIncrement: true,
      allowNull: false
    },
    scientistId: {
      type: INTEGER.UNSIGNED,
      allowNull: false
    },
    name: {
      type: STRING,
      allowNull: false
    }
  },
  {
    timestamps: false,
    sequelize: db,
    tableName: "inventions",
    indexes: [
      {
        unique: true,
        fields: ["scientistId", "name"]
      }
    ]
  }
);

Invention.belongsTo(Scientist, { as: "invention", foreignKey: "scientistId" });
Scientist.hasMany(Invention, { as: "inventions", foreignKey: "scientistId" });
export default Invention;