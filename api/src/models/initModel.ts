import ScientistService from "../services/ScientistService";
import InventionService from "../services/InventionService";
import MuseumService from "../services/MuseumService";
import moment from "moment";

export default async function initModel() {
    console.log("Running initModel script");
    
    const sci1: any = await ScientistService.create({
        label: "TSL",
        firstName: "Nikola",
        lastName: "Tesla",
        yearOfBirth: 1856,
        yearOfDeath: 1943,
        placeOfBirth: "Smiljan",
        POBLat: 45,
        POBLng: 15.3,
        nationality: "Serbian",
        science: "Engineering",
        nobelPrize: false,
        museumId: null
    });

    await InventionService.create({
        scientistId: sci1.id,
        name: "Alternating current"
    });

    await InventionService.create({
        scientistId: sci1.id,
        name: "Tesla coil"
    });

    const sci2: any = await ScientistService.create({
        label: "MLTN",
        firstName: "Milutin",
        lastName: "Milankovic",
        yearOfBirth: 1879,
        yearOfDeath: 1958,
        placeOfBirth: "Dalj",
        POBLat: 45.5,
        POBLng: 19,
        nationality: "Serbian",
        science: "Mathematics",
        nobelPrize: false,
        museumId: null
    });

    const mus1: any = await MuseumService.create({
        name: "Muzej Prirodnih Nauka",
        location: "Beograd",
        openingDate: moment("2020-06-23").unix(),
        showcasedScientistId: null,
        description: "Muzej koji otvara oci",
        showcase: false,
        showcaseStartDate: null,
        showcaseEndDate: null,
        lat: 44.8,
        lng: 20.5,
    });

    MuseumService.updateLink(sci1.id, mus1.id);

    const mus2: any = await MuseumService.create({
        name: "Narodni muzej",
        location: "Novi sad",
        openingDate: moment("2015-02-16").unix(),
        showcasedScientistId: null,
        description: "Muzej omiljen u narodu",
        showcase: false,
        showcaseStartDate: null,
        showcaseEndDate: null,
        lat: 45.2,
        lng: 19.9,
    });
}