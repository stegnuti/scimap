import { Options, Sequelize } from "sequelize";


const options: Options = {
    host: "localhost",
    dialect: "sqlite",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000,
    },
    storage: "database.sqlite"
};

const dbConfig = {
    dbName: "scimap",
    username: "root",
    password: "",
};

export default new Sequelize(
  dbConfig.dbName,
  dbConfig.username,
  dbConfig.password,
  options
);